import os

from Crypto.Util import number
import pickle

class RSA_worker():

    private_key = []
    public_key  = []
    password    = ''

    def __init__(self, priv_key_path ='', pub_key_path =''):
        self.private_key    = priv_key_path
        self.public_key     = pub_key_path

    # na dobrą sprawę ta funkcje nie powinna zapisywać klucza
    # zapis klucza powinien być na drodze serializacji
    def generateKeys(
            self,
            priv_output_path = './priv.key',
            pub_output_path = './pub.key',
            bit_length = 1024,
            password = ''
    ):
        # krok 1
        p = number.getPrime(bit_length)
        q = number.getPrime(bit_length)

        n = p * q

        # krok 2
        fi_n = (p - 1) * (q - 1)

        # krok 3
        # e = self.getNumberE(fi_n)

        # tak to może być stała
        # http://crypto.stackexchange.com/questions/3110/impacts-of-not-using-rsa-exponent-of-65537
        e = 65537

        d = self.mulinv(e, fi_n)

        return [e, n], [d, n]
        # self.public_key     = [e, n]
        # self.private_key    = [d, n]

    # zaczęrpnięte z wiki
    def xgcd(self, b, n):
        x0, x1, y0, y1 = 1, 0, 0, 1
        while n != 0:
            q, b, n = b // n, n, b % n
            x0, x1 = x1, x0 - q * x1
            y0, y1 = y1, y0 - q * y1
        return b, x0, y0

    # zaczerpnięte z wiki
    def mulinv(self, b, n):
        g, x, _ = self.xgcd(b, n)
        if g == 1:
            return x % n

    def saveKeys(self, keys, path_dir =''):
        pub_key     = keys[0]
        priv_key    = keys[1]

        if pub_key and priv_key:
            pub_key_file    = open(path_dir +  '/pub.key', 'wb')
            priv_key_file   = open(path_dir + '/priv.key','wb')

            pickle.dump(pub_key, pub_key_file, 1)
            pickle.dump(priv_key, priv_key_file, 1)
        else:
            print('No keys provided')

    def readKeys(self, path_dir = ''):
        pub_key_file        = open(path_dir + 'pub.key', 'rb')
        self.public_key     = pickle.load(pub_key_file)

        priv_key_file       = open(path_dir + 'priv.key', 'rb')
        self.private_key    = pickle.load(priv_key_file)

    # bug nie sprawdza czy liczba jest względnie pierwsza z fi_n
    def getNumberE(self, upper_bound):
        while(True):
            rand = number.getRandomRange(1, upper_bound)
            if 1 < rand < upper_bound:
                return rand

    def encrypt(self, priv_key_path):
        return 0

    def decrypt(self, pub_key_path):
        return 0

    def getFactorsNaive(self, number):
        p = 2

        # kopiowanie w razie przypadku jakby przekazywalo po referencji
        x = number

        result = []

        while(x != 1):
            if x % p != 0:
                p += 1
            else:
                result.append(p)
                x = x / p

        return result

rsa_1 = RSA_worker()
rsa_2 = RSA_worker()

currentPath = os.path.dirname(__file__)

# keys1_path = currentPath + '/keys1' + '/'
# rsa_1.readKeys(keys1_path)
# print(rsa_1.public_key)
# print(rsa_1.private_key)

# if not os.path.isdir(keys1_path):
#     os.makedirs(keys1_path)
# rsa_1.saveKeys(rsa_1.generateKeys(), os.path.abspath(keys1_path))
#
# keys2_path = currentPath + '/keys2'
# if not os.path.isdir(keys2_path):
#     os.makedirs(keys2_path)
# rsa_1.saveKeys(rsa_1.generateKeys(), os.path.abspath(keys2_path))